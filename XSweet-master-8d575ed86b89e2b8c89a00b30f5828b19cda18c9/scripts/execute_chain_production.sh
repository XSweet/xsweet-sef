#!/bin/bash
# For producing HTML5 outputs via XSweet XSLT from sources extracted from .docx (Office Open XML)

# This script is most easily invoked via the `xsweet_runner.rb` script, which will iterate through
# unzipped .docx files in a conversion directory. See https://gitlab.coko.foundation/XSweet/XSweet_runner_scripts
# for README.

# Run bash script by itself as: `sh execute_chain.sh [path_to_conversion_folder] [bookname_dir] [docx_filename]`

# 1st arg: path to a 'to_convert' directory
P=$1
# 2nd arg: name of book directory within 'to_convert' directory, e.g. "great_expectations"
BOOKNAME=$2
# 3rd arg: name of .docx file to convert, e.g. "1_introduction.docx"
DOCNAME=$3

# Note Saxon is included with this distribution, qv for license.
saxonHE="java -jar ../lib/SaxonHE9-9-1-1J/saxon9he.jar"  # SaxonHE (XSLT 3.0 processor)

# Run XSweet pipeline skipping most intermediary outputs
PIPELINE="../applications/PIPELINE-SEF.xsl"

# SERIALIZE TO HTML5
XMLTOHTML5="../applications/html-polish/html5-serialize.xsl"

$saxonHE -xsl:$PIPELINE -s:$P/$DOCNAME/word/document.xml -o:../outputs/$BOOKNAME/$DOCNAME-1PIPELINED.html
echo Made $DOCNAME-1PIPELINED.html

$saxonHE -xsl:$XMLTOHTML5 -s:../outputs/$BOOKNAME/$DOCNAME-1PIPELINED.html -o:../outputs/$BOOKNAME/$DOCNAME-2HTML5.html
echo Made $DOCNAME-HTML5.html
