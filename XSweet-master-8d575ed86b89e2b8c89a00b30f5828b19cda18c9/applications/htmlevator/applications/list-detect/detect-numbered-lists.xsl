<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="3.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:xsw="http://coko.foundation/xsweet"
  xmlns="http://www.w3.org/1999/xhtml"
  xpath-default-namespace="http://www.w3.org/1999/xhtml"

  exclude-result-prefixes="#all">

  <xsl:output method="xml" indent="no" omit-xml-declaration="yes"/>

<!--identity template-->
<!--The first template just says "keep going". It matches any time a node is processed, for which there is no better template.
In this stylesheet it is used for everything except that div, which wraps the sequence of elements that are to be grouped. (The first grouping is between lists and not-lists, then lists are subgrouped as well.)-->
  <xsl:template match="node() | @*">
    <xsl:copy>
      <xsl:apply-templates select="node() | @*"/>
    </xsl:copy>
  </xsl:template>

  <!-- This comes out of XSweet. -->
  <xsl:template match="div[@class='docx-body']">
    <xsl:copy>
      <xsl:apply-templates select="@*"/> <!--the select copies the docx-body div attribute (class="docx-body"). w/o it the div gets copied but not the class-->
      <xsl:call-template name="detect-lists"/> <!--calls detect-lists on all the children of <div class="docx-body">-->
    </xsl:copy>
  </xsl:template>

  <!--detect-list template emits a sequence of groups, each containing a cluster of elements belonging to a list.
  Unmarked list detection doesn't consider list level: `<xsw:list>`s get @level 0, although list item retains its `data-xsweet-list-level` attribute
  Grouping key is true() for nominal list items, false if not.-->
  <xsl:template name="detect-lists">
    <xsl:for-each-group select="*" group-adjacent="xsw:is-list(.)">
      <xsl:choose>
        <!--lists must have at least 2 adjacent <p>s that look like list items-->
        <xsl:when test="current-grouping-key() and count(current-group()) gt 1">
          <xsl:call-template name="tag-lists"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:apply-templates select="current-group()"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:for-each-group>
  </xsl:template>
  
  <xsl:template name="tag-lists">
    <xsw:list level="0">
      <xsl:apply-templates select="current-group()"/>
    </xsw:list>
  </xsl:template>
  
  <xsl:function name="xsw:is-list" as="xs:boolean?">
    <xsl:param name="whose" as="element()"/>
    <!--list detected if paragraph matches: any amt of ws at start + digits + a period + at least one space-->
    <xsl:sequence select="matches($whose,'^\s*\d+\.\s')"/>
  </xsl:function>

</xsl:stylesheet>