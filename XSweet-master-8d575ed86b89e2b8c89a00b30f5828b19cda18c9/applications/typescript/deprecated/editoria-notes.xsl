<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="3.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:xsw="http://coko.foundation/xsweet"
  xpath-default-namespace="http://www.w3.org/1999/xhtml"
  xmlns="http://www.w3.org/1999/xhtml"
  exclude-result-prefixes="#all">

  <xsl:output method="xml" indent="no" omit-xml-declaration="yes"/>
  
  <!-- Inoperative XSLT! can be retired/removed -->
  <xsl:template match="/">
    <xsl:copy-of select="/"/>
  </xsl:template>
  
  <xsl:template match="html">
    <xsl:next-match/>
  </xsl:template>
  

</xsl:stylesheet>
