Based on XSweet_runner_scripts repo. Contains copies of XSweet, HTMLevator, and Editoria Typescript, with `.sef` files generated in the `applications` directory next to their `.xsl` counterparts.
* Run the `.sef` pipeline with `ruby xsweet_runner.rb`
* Run the faster invoke-all-other-sheets-from-within-one pipeline, which still uses `.sef`s, with `ruby xsweet_runner_prod_time.rb`. This script runs it 30 times and prints runtimes, then an average.

Notes:
* `sef`s created using Saxon HE as the target.
* You'll have to do a tiresome project-wide find/replace to replace `uri`s inside `.sef`s for `.xsl`s that invoke others inside them before the pipeline will run, as compiling to `.sef` inserts absolute references.
* The pipeline is mostly but not exclusively `.sef`.
