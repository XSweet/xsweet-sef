starting = Time.now

xsweet_script_path = Dir.glob("XSweet-*").pop

rootDir = Dir.getwd
convertDir = rootDir + "/to_convert"

book_list = Dir["#{convertDir}/*"].select {|cand| File.directory? cand}
book_list.delete("#{convertDir}/temp")

puts "BOOK LIST"
number_label = 1
book_list.each do |book|
  puts "#{number_label}: #{book}"
  number_label += 1
end
puts "BOOK LIST END"

starting = Time.now
times = 30
times.times {
  tempTime = Time.now
  book_list.each do |book|
    book = book.split("/").last
    # puts "BOOK: #{book}"

    file_path_list = Dir["#{convertDir}/#{book}/*"].select {|cand| File.directory? cand}
    # puts file_path_list

    file_list = []
    file_path_list.each do |file_path|
      file_list << file_path.split("/").last
    end

    newDir = rootDir + "/#{xsweet_script_path}/scripts"

    Dir.chdir newDir

    file_list.each do |chapter|
      # puts "converting: #{chapter}"
      %x`sh execute_chain_production.sh #{convertDir}/#{book} #{book} #{chapter}`
    end

    # puts "done with #{book}"
  end
  puts Time.now - tempTime
}
ending = Time.now
elapsed = ending - starting
puts elapsed / times
